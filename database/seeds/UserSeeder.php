<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate([
            'email' => 'lucioflaviom@hotmail.com'
        ], [
            'name' => 'LUCIO FLAVIO MIRANDA DE MEDEIROS',
            'password' => Hash::make('123456'),
            'social_id' => '00747227438',
            'account_type' => 'personal',
            'sienge_id' => 221
        ]);

        User::firstOrCreate([
            'email' => 'email@jeffsantos.com.br'
        ], [
            'name' => 'DAMIAO ISRAEL MADALENA DA SILVA',
            'password' => Hash::make('123456'),
            'social_id' => '05622630479',
            'account_type' => 'personal',
            'sienge_id' => 5000
        ]);

        User::firstOrCreate([
            'email' => 'waldemilson_34012@trt5.jur.br'
        ], [
            'name' => 'WALDEMILSON MORAES DOS SANTOS SCHUFFNER',
            'password' => Hash::make('123456'),
            'social_id' => '33353611568',
            'account_type' => 'personal',
            'sienge_id' => 5056
        ]);

        User::firstOrCreate([
            'email' => 'frankwellingto057@gmail.com'
        ], [
            'name' => 'WELLINGTON FRANK FERREIRA SILVA',
            'password' => Hash::make('123456'),
            'social_id' => '03461531588',
            'account_type' => 'personal',
            'sienge_id' => 5041
        ]);

        User::firstOrCreate([
            'email' => 'jefflssantos@gmail.com'
        ], [
            'name' => 'CELSON LUIS FERREIRA DE LIMA JÚNIOR',
            'password' => Hash::make('123456'),
            'social_id' => '06828836464',
            'account_type' => 'personal',
            'sienge_id' => 5055
        ]);
    }
}
