@component('mail::message')
# Olá {{ explode(' ', $user->name)[0] }}!</strong>

<p>Aqui está sua nova senha: <strong>{{ $password }}</strong></p>

<p>Equipe da Satya Soluções</p>
@endcomponent
