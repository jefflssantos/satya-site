@component('mail::message')
# Olá {{ explode(' ', $user->name)[0] }},</strong>

<p>
  Ficamos muito felizes pelo seu interesse de cadastro na nossa Área do cliente.
  Para acessar o sistema e validar o seu cadastro, clique no botão abaixo e insira sua senha:
</p>

@component('mail::button', ['url' => route('login')])
  ACESSE
@endcomponent
<p>Senha: <strong>{{ $password }}</strong></p>

<p>Equipe da Satya Soluções</p>
@endcomponent
