@component('mail::message')
# Formulário de contato

<h4 style="margin-bottom: 0">Nome</h4>
{{ $user->name }}

@if($user->phone)
<h4 style="margin-bottom: 0">Telefone</h4>
{{ $user->phone }}
@endif

<h4 style="margin-bottom: 0">{{ strtoupper(social_id_type($user->social_id)) }}</h4>
{{ $user->social_id }}

<h4 style="margin-bottom: 0">Assunto</h4>
{{ $subject }}

<h4 style="margin-bottom: 0">Mensagem</h4>
{{ $message }}

@endcomponent
