@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12 col-md-10 col-lg-8">
        @include('layouts._page_title', [
          'title' =>'Contato',
          'subtitle' => 'Acompanhe todos os seus boletos e fique por dentro de todas as suas transações'
        ])

        @include('layouts._alert')

        <form action="{{ route('contact.store') }}" method="POST">
          @csrf

          <div class="form-group py-2">
            <label>Nome</label>
            <input type="text" class="form-control" value="{{ Auth::user()->name }}" readonly>
          </div>

          @if(Auth::user()->phone)
            <div class="form-group py-2">
              <label>Telefone</label>
              <input type="text" class="form-control mask-phone" value="{{ Auth::user()->phone }}" readonly>
            </div>
          @endif

          <div class="form-group py-2">
            <label>{{ strtoupper(social_id_type(Auth::user()->social_id)) }}</label>
            <input type="text" class="form-control mask-{{ social_id_type(Auth::user()->social_id) }}" value="{{ Auth::user()->social_id }}" readonly>
          </div>

          <div class="form-group">
            <select name="subject" class="form-control @error('subject') is-invalid @enderror">
              <option value="">Assunto</option>
              @foreach(config('contact_form.options') as $key => $option)
                <option value="{{ $key }}" @if(old('subject') == $key) selected @endif>{{ $option['title'] }}</option>
              @endforeach
            </select>
            @error('subject')
              <small class="text-danger form-text">{{ $message }}</small>
            @enderror
          </div>

          <div class="form-group @error('message') is-invalid @enderror">
            <textarea name="message" class="form-control" rows="8" placeholder="Mensagem">{{ old('message') }}</textarea>

            @error('message')
              <small class="text-danger form-text">{{ $message }}</small>
            @enderror
          </div>
          <div class="d-flex justify-content-between">
{{--                <a href="{{ route('password.request') }}" class="btn btn-link px-0 text-muted">Esqueci minha senha?</a>--}}
            <input type="submit" class="btn btn-secondary text-white hover-info" value="Enviar">
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
