@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12 col-md-10 col-lg-8">
        @include('layouts._page_title', [
          'title' =>'Seu perfil',
          'subtitle' => 'Matenha seu perfil sempre atualizado.'
        ])

        @include('layouts._alert')

        <div class="form-group py-2">
          <label>Nome</label>
          <input type="text" class="form-control" value="{{ Auth::user()->name }}" readonly>
        </div>

        @if(Auth::user()->phone)
          <div class="form-group py-2">
            <label>Telefone</label>
            <input type="text" class="form-control mask-phone" value="{{ Auth::user()->phone }}" readonly>
          </div>
        @endif

        <div class="form-group py-2">
          <label>{{ strtoupper(Auth::user()->social_id_type) }}</label>
          <input type="text" class="form-control mask-{{ Auth::user()->social_id_type }}" value="{{ Auth::user()->social_id }}" readonly>
        </div>

        @if(Auth::user()->full_address)
          <div class="form-group py-2">
            <label>Endereço</label>
            <input type="text" class="form-control" value="{{ Auth::user()->full_address }}" readonly>
          </div>
        @endif
        <div class="form-group py-2">
          <label>Email</label>
          <input type="text" class="form-control" value="{{ Auth::user()->email }}" readonly>
        </div>

        <form action="{{ route('profile.store') }}" method="POST">
          @csrf

          <h5 class="mb-2"><strong>Definir uma nova senha</strong></h5>
          <div class="form-group">
            <input type="password" name="new_password" class="form-control" placeholder="Digite sua nova senha">
            @error('new_password')
              <small class="text-danger form-text">{{ $message }}</small>
            @enderror
          </div>

          <div class="form-group">
            <input type="password" name="new_password_confirmation" class="form-control" placeholder="Confirmar senha">
            @error('new_password_confirmation')
              <small class="text-danger form-text">{{ $message }}</small>
            @enderror
          </div>



          <div class="w-100 text-right mb-3">
            <input type="submit" class="btn btn-primary" value="ATUALIZAR SENHA">
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
