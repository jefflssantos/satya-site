<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  @include('layouts._favicon')

  <link rel="dns-prefetch" href="//fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@400;800&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.0/animate.min.css">

  <link rel="stylesheet" href="{{ mix('css/bootstrap.css') }}">
  <link rel="stylesheet" href="{{ mix('css/app.css') }}">

  <title>{{ config('app.name') }}</title>
</head>
<body>
  <div class="d-flex">
    @include('layouts._sidebar')

    <div class="w-100">
      @include('layouts._navbar')

      <div class="_content">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-12 col-lg-10 _content_inside">
              @yield('content')
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" tabindex="-1" id="loading-modal" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body text-center py-5">
          <img src="{{ asset('imgs/brand_two.svg') }}" alt="Satya Logotipo" class="img-fluid">
          <h3 class="my-4">Carregando...</h3>
        </div>
      </div>
    </div>
  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" defer></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" defer></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" defer></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/print-js/1.1.0/print.min.js" defer></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" defer></script>
  <script src="{{ mix('js/app.js') }}" defer></script>
</body>
</html>
