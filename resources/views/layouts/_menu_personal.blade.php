<li class="profile pl-5 mb-3 d-lg-none">
  <h3 class="text-white mb-3">{{ Auth::user()->name }}</h3>
  <a href="{{ route('profile.show') }}" class="btn btn-default btn-outline-light btn-sm d-inline-block">
    <strong>EDITAR PERFIL</strong>
  </a>
</li>
<li>
  <a href="{{ route('personal.bills.index') }}">
    <img src="{{ asset('imgs/icons/paper.svg') }}">
    <p class="d-lg-none">
      2ª Via de Boletos
      <img src="{{ asset('imgs/icons/arrow-right.svg') }}" class="ml-1">
    </p>
  </a>
</li>
<li>
  <a href="{{ route('personal.bills.history') }}">
    <img src="{{ asset('imgs/icons/chart.svg') }}">
    <p class="d-lg-none">
      Histórico de Pagamentos
      <img src="{{ asset('imgs/icons/arrow-right.svg') }}" class="ml-1">
    </p>
  </a>
</li>
<li>
  <a href="{{ route('personal.income_tax.index') }}">
    <img src="{{ asset('imgs/icons/bank.svg') }}">
    <p class="d-lg-none">
      Imposto de Renda
      <img src="{{ asset('imgs/icons/arrow-right.svg') }}" class="ml-1">
    </p>
  </a>
</li>
<li>
  <a href="{{ route('contact.create') }}">
    <img src="{{ asset('imgs/icons/mail.svg') }}">
    <p class="d-lg-none">
      Contato
      <img src="{{ asset('imgs/icons/arrow-right.svg') }}" class="ml-1">
    </p>
  </a>
</li>
<li class="d-md-none">
  <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
    <img src="{{ asset('imgs/icons/power.svg') }}" style="
    width: 1.7rem;
    -webkit-filter: invert(100%);
    filter: invert(100%);
">
    <p class="d-lg-none">
      Encerrar sessão
      <img src="{{ asset('imgs/icons/arrow-right.svg') }}" class="ml-1">
    </p>
  </a>
</li>
