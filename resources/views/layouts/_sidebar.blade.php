<div class="sidebar d-lg-block px-4 animate__animated w-sm-100" style="display: none">
  <a href="#" class="menu-icon" id="extend-sidebar">
    <img src="{{ asset('imgs/icons/hamburger.svg') }}">
  </a>

  <ul class="navbar-nav sidebar-menu">
    @include('layouts._menu_personal')
  </ul>
</div>
