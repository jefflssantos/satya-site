<div class="container my-5">
  <h1 class="text-info mb-3">{{ $title }}</h1>
  <p class="text-primary big">{{ $subtitle }}</p>
</div>
