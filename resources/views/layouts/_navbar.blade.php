<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-primary">
  <div class="container-fluid">
    <a href="#" class="ml-2 ml-lg-0 mr-2 pr-3 d-lg-none" id="open-menu-mobile">
      <img src="{{ asset('imgs/icons/hamburger.svg') }}">
    </a>
    <a href="#" class="ml-3 mr-4 pr-1" id="close-menu-mobile" style="display: none">
      <img src="{{ asset('imgs/icons/close-expended-menu.svg') }}">
    </a>
    <a href="#" class="d-none d-lg-block mr-4 pr-1" id="contract-menu" style="display: none">
      <img src="{{ asset('imgs/icons/close-expended-menu.svg') }}">
    </a>
    <a class="navbar-brand mr-auto ml-lg-4" href="/">
      <img src="{{ asset('imgs/brand.svg') }}" alt="Satya Logotipo">
    </a>
    <div class="collapse navbar-collapse" >
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <form id="logout-form" action="{{ route('logout') }}" method="POST">
            @csrf
            <button type="submit" class="btn btn-default btn-outline-light btn-sm"><strong>ENCERRAR SESSÃO</strong></button>
          </form>
        </li>
      </ul>
    </div>
  </div>
</nav>
