<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  @include('layouts._favicon')

  <link rel="dns-prefetch" href="//fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@400;800&display=swap" rel="stylesheet">

  <link rel="stylesheet" href="{{ mix('css/bootstrap.css') }}">
  <link rel="stylesheet" href="{{ mix('css/app.css') }}">

  <title>{{ config('app.name') }}</title>
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-primary">
    <div class="container-fluid">
      <a class="navbar-brand" href="/">
        <img src="{{ asset('imgs/brand.svg') }}" alt="Satya Logotipo">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto my-3 my-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="/">Sobre a empresa</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/">Serviços</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/">Blog</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/">Fale conosco</a>
          </li>
          <li class="nav-item ml-lg-2 mt-2 mt-lg-0">
            <a class="btn btn-default btn-outline-light btn-sm" href="{{ route('login') }}"><strong>ACESSO RESTRITO</strong></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  @yield('content')

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" defer></script>
  <script src="{{ mix('js/app.js') }}" defer></script>
</body>
</html>
