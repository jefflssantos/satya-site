@extends('layouts.auth')

@section('content')

<div class="container py-lg-5">
  <div class="row my-lg-5 py-5">
    <div class="col-12 col-lg-6">
      <div class="d-flex align-self-center mb-4 mb-lg-5">
        <img src="{{ asset('imgs/icons/client-area-line-home.svg') }}" class="mr-4 mb-1">
        <small class="d-flex align-items-center">
          <strong class="text-muted">ÁREA DO CLIENTE</strong>
        </small>
      </div>
      <h1 class="text-info">Acesse sua conta</h1>
      <h1 class="text-primary font-weight-lighter">
        tenha acesso a suas movimentações, histórico, etc
      </h1>
    </div>
    <div class="col-12 col-lg-6 mt-5 mt-lg-3">
      <h3 class="mb-3 text-info">Suas credenciais</h3>

      @include('auth._alert')

      <form action="{{ route('login') }}" method="POST" class="pb-4">
        @csrf

        <div class="form-group @error('social_id') is-invalid @enderror">
          <input type="text" name="social_id" class="form-control mask-cpf-cnpj" placeholder="CPF ou CNPJ" required>
        </div>

        <div class="form-group @error('password') is-invalid @enderror">
          <input type="password" name="password" class="form-control" placeholder="Senha" required>
        </div>

        <div class="d-flex justify-content-between">
          <a href="{{ route('password.request') }}" class="btn btn-link px-0 text-muted">Esqueci minha senha?</a>
          <input type="submit" class="btn btn-primary text-white font-weight-bold" value="ENTRAR">
        </div>
      </form>

      <h3 class="mt-5 mb-4 text-info">Você ainda não é cadastrado?</h3>
      <a href="{{ route('register') }}" class="btn btn-secondary text-white font-weight-bold py-4">CADASTRAR</a>
    </div>
  </div>
</div>

@include('auth._footer')

@endsection
