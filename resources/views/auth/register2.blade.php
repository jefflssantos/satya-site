@extends('layouts.auth')

@section('content')

<div class="container py-lg-5">
  <div class="row my-lg-5 py-5">
    <div class="col-12 col-lg-6">
      <h1 class="text-info">Cadastre sua conta</h1>
      <h1 class="text-primary font-weight-lighter">
        tenha acesso a suas movimentações, histórico, etc
      </h1>
    </div>
    <div class="col-12 col-lg-6 mt-5 mt-lg-3">
      <h3 class="mb-3 text-info">Entre com CPF ou CNPJ</h3>
      <form action="{{ route('register') }}" method="POST">
        @csrf

        <div class="form-group @error('social_id') is-invalid @enderror">
          <input type="text" name="social_id" class="form-control mask-cpf-cnpj" value="{{ old('social_id') }}">

          @error('social_id')
            <small class="text-danger form-text">{{ $message }}</small>
          @enderror
        </div>

        @include('auth._alert')

        <div class="d-flex justify-content-end">
          <input type="submit" class="btn btn-primary text-white hover-info" value="CONTINUAR">
        </div>
      </form>
    </div>
  </div>
</div>

@include('auth._footer')

@endsection
