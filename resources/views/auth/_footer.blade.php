<footer class="container">
  <div class="row">
    <div class="col d-flex align-items-start">
      <img src="{{ asset('imgs/brand_two.svg') }}" class="img-fluid mr-5">
      <div>
        <p class="mb-2">83 | 9 8734-8463</p>
        <p>83 | 9 9934-0002</p>
      </div>
    </div>
  </div>
  <div class="row py-5">
    <div class="col-8">
      Satya © {{ date('Y') }} | <a href="#">Política de Privacidade</a>
    </div>
    <div class="col-4 text-right">
      <a href="https://www.qualitare.com/" target="_blank">
        <img src="{{ asset('imgs/made_by_qualitare_brand.svg') }}" class="img-fluid">
      </a>
    </div>
  </div>
</footer>
