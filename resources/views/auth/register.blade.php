@extends('layouts.auth')

@section('content')

  <div class="container py-lg-5">
    <div class="row my-lg-5 py-5">
      <div class="col-12 col-lg-6">
        <div class="d-flex align-self-center mb-4 mb-lg-5">
          <img src="{{ asset('imgs/icons/client-area-line-home.svg') }}" class="mr-4 mb-1">
          <small class="d-flex align-items-center">
            <strong class="text-muted">ÁREA DO CLIENTE</strong>
          </small>
        </div>
        <h1 class="text-info">Crie sua conta</h1>
        <h1 class="text-primary font-weight-lighter">
          tenha acesso a suas movimentações, histórico, etc
        </h1>
      </div>
      <div class="col-12 col-lg-6 mt-5 mt-lg-3 pt-lg-5">
        <h3 class="mb-3 text-info mt-lg-3">Entre com suas informações</h3>

        @include('auth._alert')

        <form action="{{ route('register') }}" method="POST">
          @csrf

          <div class="form-group">
            <input type="text" name="social_id" class="form-control mask-cpf-cnpj" placeholder="CPF ou CNPJ" minlength="14" required>
          </div>

          <div class="d-flex justify-content-between">
            <a href="{{ route('login') }}" class="btn btn-link px-0 text-muted">Lembrei minha senha</a>
            <input type="submit" class="btn btn-primary text-white hover-info" value="CONTINUAR">
          </div>
        </form>
      </div>
    </div>
  </div>

  @include('auth._footer')

@endsection
