@if ($message = Session::get('success'))
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{ $message }}</strong>
  </div>
@endif

@if ($errors = Session::get('errors'))
  <div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    @foreach($errors->all() as $message)
      <strong>{{ $message }}</strong>
    @endforeach
  </div>
@endif
