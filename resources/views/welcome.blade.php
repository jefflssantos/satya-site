@extends('layouts.app')

@section('content')
  @if(Session::has('noSalesContract'))
    <div class="alert alert-info alert-block position-absolute px-5 mt-4">
      <strong>Seu login não possui pagamento ativo conosco. <br> Qualquer dúvida, entre em contato com a administração da Satya.</strong>
    </div>
  @endif

  <div class="col-12 col-xl-7 align-self-center" style="position: relative;">
    <div style="position: absolute; top: 100%;-ms-transform: translateY(100%);transform: translateY(100%);">
      <div class="d-flex align-self-center" >
        <img src="{{ asset('imgs/icons/client-area-line-home.svg') }}" class="mr-4 mb-1">
        <small class="d-flex align-items-center">
          <strong class="text-muted">ÁREA DO CLIENTE</strong>
        </small>
      </div>
      <h1 class="text-info my-3">Bem vindo, {{ ucfirst(mb_strtolower(explode(' ', Auth()->user()->name)[0])) }}</h1>
      <h2 class="text-primary big" style="line-height: 1.3">na seção ao lado você pode acessar seu histórico, boletos, IR e ainda tirar dúvidas conosco</h2>
    </div>
  </div>
  <img src="{{ url('imgs/welcome.svg') }}" class="d-none d-xl-block" style="position: fixed; right: 0;top: 50%; transform: translate(0, -50%);">
@endsection
