@extends('layouts.app')

@section('content')
  @include('layouts._page_title', [
    'title' =>'Extrato de imposto de renda',
    'subtitle' => 'Acompanhe todos os extratos de Impostos de Renda e confira sua situação junto ao Leão'
  ])

  <div class="container">
{{--    @include('layouts._alert')--}}

{{--    <form action="{{ route('personal.income_tax.request') }}" method="POST">--}}
{{--      @csrf--}}
{{--      <label>Ano calendário</label>--}}
{{--      <div class="form-group py-3">--}}
{{--        <select name="year" class="form-control" required>--}}
{{--          <option value="">Selecione</option>--}}
{{--          @foreach(range(1, 6) as $i)--}}
{{--            <option value="{{ $year = date("Y", strtotime("-{$i} year")) }}">{{ $year }}</option>--}}
{{--          @endforeach--}}
{{--        </select>--}}
{{--      </div>--}}

{{--      <button type="submit" class="btn btn-default btn-outline-primary"><strong>ENVIAR POR EMAIL</strong></button>--}}
{{--    </form>--}}

    <h3>Aguarde, essa funcionalidade estará disponível em breve.</h3>
  </div>
@endsection
