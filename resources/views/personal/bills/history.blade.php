@extends('layouts.app')

@section('content')
  @include('layouts._page_title', [
    'title' =>'Histórico de pagamentos',
    'subtitle' => 'Acompanhe todos os seus boletos e fique por dentro de todas as suas transações'
  ])

 @include('personal.bills._list', ['installments' => $installments, 'title' => 'Seus Boletos'])
@endsection
