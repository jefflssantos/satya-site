@extends('layouts.app')

@section('content')
  @include('layouts._page_title', [
    'title' =>'2ª Via dos boletos',
    'subtitle' => 'Acompanhe todos os seus boletos e fique por dentro de todas as suas transações'
  ])

  <div class="container">
    <form action="{{ request()->url() }}" data-submit-on-change="true" class="form-row">
      <div class="form-group p-0 mr-2">
        <select name="bill" class="form-control form-control-sm select-filter">
          @foreach($salesContracts as $contracts)
            <option value="{{ $contracts['receivableBillId'] }}" @if($contracts['receivableBillId'] == request()->bill) selected @endif>{{ $contracts['number'] }}</option>
          @endforeach
        </select>
      </div>

      @if ($salesContract['situation'] == 'Emitido')
        <div class="form-group p-0">
          <select name="status" class="form-control form-control-sm select-filter">
            <option value="">Status</option>
            @foreach($installmentStatus as $key => $status)
              <option value="{{ $key }}" @if($key == request()->status) selected @endif>@lang("status.{$key}")</option>
            @endforeach
          </select>
        </div>
      @endif
    </form>
  </div>

  @if($salesContract['situation'] == 'Emitido')
    @includeWhen($latestInstallments->count(), 'personal.bills._list', ['installments' => $latestInstallments, 'title' => 'Últimos 90 dias'])
    <div class="col-12 mt-5"></div>
    @includeWhen($installments->count(), 'personal.bills._list', ['installments' => $installments, 'title' => 'Outros boletos'])
  @else
    <h3 class="text-center mt-5">Não é possível visualizar conteúdo.</h3>
    <h3 class="text-center mt-2">Para mais informações, favor entrar em contato com a Satya.</h3>
  @endif
@endsection
