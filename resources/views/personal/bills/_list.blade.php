<div class="container">
  <h4 class="text-primary mb-4">{{ $title }}</h4>

  <div class="container">
    <div class="row">
      <div class="col-4 col-md-3 col-lg-2 small"><strong>COD.</strong></div>
      <div class="col-4 col-md-3 col-lg-3 small"><strong>DATA</strong></div>
      <div class="col-4 col-md-3 col-lg-3 small"><strong>SITUAÇÃO</strong></div>
      <div class="col-6 col-md-3 col-lg-2 small text-lg-left mt-2 mt-md-0"><strong>VALOR</strong></div>
      <div class="col-12 col-lg-2 small d-none d-lg-block text-right"><strong>DETALHES</strong></div>
    </div>

    @foreach($installments as $installment)
      <div class="row _line">
        <div class="col-4 col-md-3 col-lg-2">{{ $installment['documentId'] }}{{ $installment['billReceivableId'] }}-{{ $installment['id'] }}</div>
        <div class="col-4 col-md-3 col-lg-3">{{ $installment['dueDate']->format('d/m/Y') }}</div>
        <div class="col-4 col-md-3 col-lg-3">
          @if($installment['status'] == 'paid')
            <span class="dot-primary"></span>Pago
          @elseif($installment['status'] == 'toPay')
            <span class="dot-warning"></span>A pagar
          @else
            <span class="dot-danger"></span>Pendente
          @endif
        </div>
        <div class="col-6 col-md-3 col-lg-2 text-lg-left">{{ $installment['originalValue'] }}</div>

        {{-- Mobile Icons--}}
        @if($installment['status'] != 'paid')
          <div class="col-12 d-lg-none my-1 my-md-2">
            <a href="{{ $installmentLink = route('personal.bills.show', [$installment['billReceivableId'], $installment['id'], $installment['dueDate']->format('d-m-Y')]) }}" >
              <img src="{{ asset('imgs/icons/download.svg') }}" class="hover-blue">
            </a>
          </div>
        @endif
        {{-- End Mobile Icons--}}

        {{-- Desktop Icons--}}
        <div class="d-none d-lg-block col-lg-2 text-right my-lg-0">
          @if($installment['status'] == 'paid')
            <img src="{{ asset('imgs/icons/view.svg') }}" class="mx-2 mx-lg-3 opacity-4">
            <img src="{{ asset('imgs/icons/download.svg') }}" class="opacity-4">
          @else
            <a href="{{ $installmentLink }}" target="_blank" class="mx-2 mx-lg-3">
              <img src="{{ asset('imgs/icons/view.svg') }}" class="hover-blue">
            </a>
            <a href="#" data-print-bill="{{ $installmentLink }}">
              <img src="{{ asset('imgs/icons/download.svg') }}" class="hover-blue">
            </a>
          @endif
        </div>
        {{-- End Desktop Icons--}}
      </div>
    @endforeach
  </div>
</div>
