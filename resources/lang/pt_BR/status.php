<?php

return [
    'toPay' => 'A pagar',
    'pastDue' => 'Pendente',
    'paid' => 'Pago',
];
