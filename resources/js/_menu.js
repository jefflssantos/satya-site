$('#open-menu-mobile').click(function(e) {
  e.preventDefault();

  $(this).hide();

  $('.sidebar')
    .removeClass('animate__fadeOutLeft')
    .addClass('animate__fadeInLeft w-100')
    .fadeIn()

  $('#close-menu-mobile').fadeIn();
})

$('#close-menu-mobile').click(function(e) {
  e.preventDefault();

  $(this).hide();

  $('.sidebar')
    .removeClass('animate__fadeInLeft')
    .addClass('animate__fadeOutLeft')
    .fadeOut();

  $('#open-menu-mobile').fadeIn();
})

$('#extend-sidebar').click(e => {
  e.preventDefault();

  document.cookie = "sidebar=opened";

  $('.sidebar')
    .removeClass('animate__fadeInLeft')
    .addClass('animate__fadeOutLeft');

  setTimeout(() => {
    let $sidebar = $('.sidebar');

    $sidebar
      .css('z-index', 1030)
      .css('min-width', '350px')
      .find('.d-lg-none')
      .attr('style', 'display: block!important');

    $sidebar
      .removeClass('animate__fadeOutLeft')
      .addClass('animate__fadeInLeft')
      .show();

    $('._content')
      .css('width', 'calc(100% - 350px)')
      .css('margin-left', '350px')

    $('._content_inside')
      .removeClass('col-lg-10')
      .addClass('col-lg-12')
  }, 500)
});

$('#contract-menu').click(e => {
  e.preventDefault();

  document.cookie = "sidebar=closed";

  $('.sidebar')
    .removeClass('animate__fadeInLeft')
    .addClass('animate__fadeOutLeft');

  setTimeout(() => {
    let $sidebar = $('.sidebar');

    $sidebar
      .css('z-index', 1031)
      .css('min-width', '')
      .find('.d-lg-none')
      .attr('style', 'display: none');

    $sidebar
      .removeClass('animate__fadeOutLeft')
      .addClass('animate__fadeInLeft')
      .show();

    $('._content')
      .css('width', 'calc(100% - 68px)')
      .css('margin-left', '68px')

    $('._content_inside')
      .removeClass('col-lg-12')
      .addClass('col-lg-10')
  }, 500)
});

if (getCookie('sidebar') == 'opened' && window.innerWidth > 990) {
  $('#extend-sidebar').click();
}

function getCookie(name) {
  let re = new RegExp(name + "=([^;]+)");
  let value = re.exec(document.cookie);
  return (value != null) ? unescape(value[1]) : null;
}
