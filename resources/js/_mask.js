$('.mask-cpf').mask('000.000.000-00')
$('.mask-cnpj').mask('00.000.000/0000-00')

const maskPhoneBehavior = function (val) {
  return val.replace(/\D/g, '').length <= 10 ? '(00) 0000-00009' : '(00) 0 0000-0000';
};

$('.mask-phone').mask(maskPhoneBehavior, {
  onKeyPress: function(val, e, field, options) {
    field.mask(maskPhoneBehavior.apply({}, arguments), options);
  }
});

let $socialIdInput = $('.mask-cpf-cnpj');
let options = {
  onKeyPress: function (cpf, ev, el, op) {
    let masks = ['000.000.000-000', '00.000.000/0000-00'];
    $socialIdInput.mask((cpf.length > 14) ? masks[1] : masks[0], op);
  }
}

$socialIdInput.length > 11
  ? $socialIdInput.mask('00.000.000/0000-00', options)
  : $socialIdInput.mask('000.000.000-00#', options);
