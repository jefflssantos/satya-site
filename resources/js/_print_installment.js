$('[data-print-bill]').click(function (e) {
  e.preventDefault();
  let modal = $('#loading-modal')
  modal.modal();

  printJS({
    printable: $(this).attr('data-print-bill'),
    onLoadingEnd: function () {
      modal.modal('hide');
    }
  });
})
