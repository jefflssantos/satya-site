<?php

namespace App\Repositories;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class Repository
{
    protected string $method = Request::METHOD_GET;
    protected string $endpoint;
    protected array $body = [];
    protected array $query = [];
    protected int $ttl = 60;

    public function fetch(): array
    {
        if (! config('sienge.api.cache') || $this->method != Request::METHOD_GET) {
            return $this->request();
        }

        $key = base64_encode(Auth::id() . $this->getUrl());

        return Cache::remember($key, $this->ttl, function() {
            return $this->request();
        });
    }

    private function getUrl(): string
    {
        $url = config('sienge.api.url');
        $query = http_build_query($this->query);

        return "{$url}/{$this->endpoint}?{$query}";
    }

    private function request(): array
    {
        if ($this->method == Request::METHOD_GET) {
            $this->body = $this->query;
        }

        $response = $this->client()->{$this->method}($this->getUrl(), $this->body);

        if ($response->failed()) {
            Log::critical('Sienge api error', [$response->body()]);
            abort(
                502,
                'Tivemos um problema ao buscar seus dados, tente novamente em alguns minutos.'
            );
        }

        return $response->json() ?? [];
    }

    private function client(): PendingRequest
    {
        return Http::withBasicAuth(
            config('sienge.api.user'),
            config('sienge.api.pass'),
        );
    }
}
