<?php

namespace App\Repositories\Personal;

use Illuminate\Support\Arr;
use NumberFormatter;
use App\Repositories\Repository;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class BillRepository extends Repository
{
    private NumberFormatter $moneyFormatter;

    private const INSTALLMENT_STATUS = [
        'paid' => 'paidInstallments',
        'pastDue' => 'dueInstallments',
        'toPay' => 'payableInstallments'
    ];

    public function __construct()
    {
        $this->moneyFormatter = new NumberFormatter('pt_BR', NumberFormatter::CURRENCY);
    }

    public function get(string $socialId): Collection
    {
        $this->endpoint = 'current-debit-balance';
        $this->query = [social_id_type($socialId) => $socialId];

        $bills = $this->fetch()['results'] ?? [];

        return $this->transform($bills);
    }

    public function paidInstallments(string $socialId): Collection
    {
        $installments = $this->get($socialId);

        return $installments->where('status', 'paid')->sortByDesc('dueDate');
    }

    public function installmentLink(int $bill, int $installment): ?string
    {
        $this->endpoint = 'payment-slip-notification/';
        $this->query = ['billReceivableId' => $bill, 'installmentId' => $installment];

        $installment = $this->fetch()['results'];

        return Arr::first($installment)['urlReport'] ?? null;
    }

    protected function transform(array $bills): Collection {
        foreach ($bills as $bill) {
            foreach (self::INSTALLMENT_STATUS as $status => $index) {
                foreach($bill[$index] ?? [] as $installment) {
                    $installment['id'] = $installment['installmentId'];
                    $installment['status'] = $status;
                    $installment['dueDate'] = Carbon::createFromFormat('Y-m-d', $installment['dueDate']);
                    $installment['billReceivableId'] = $bill['billReceivableId'];
                    $installment['documentId'] = $bill['documentId'];
                    $installment['originalValue'] = $this
                        ->moneyFormatter->formatCurrency($installment['originalValue'], 'BRL');

                    $data[] = $installment;
                }
            }
        }

        return collect($data ?? []);
    }
}
