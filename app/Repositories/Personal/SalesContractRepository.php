<?php

namespace App\Repositories\Personal;

use App\Repositories\Repository;
use App\User;
use Illuminate\Support\Collection;

class SalesContractRepository extends Repository
{
    protected int $siengeId;
    protected int $ttl = 60 * 60;

    public function __construct(int $siengeId)
    {
        $this->siengeId = $siengeId;
    }

    public function get(): ?Collection
    {
        $this->endpoint = 'sales-contracts';
        $this->query = ['customerId' => $this->siengeId];

        if ($response = $this->fetch()['results']) {
            return collect($response);
        }

        return null;
    }
}
