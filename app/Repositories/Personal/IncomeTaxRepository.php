<?php

namespace App\Repositories\Personal;

use App\Repositories\Repository;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class IncomeTaxRepository extends Repository
{
    public function get(int $year, array $companies): void
    {
        $this->method = Request::METHOD_POST;
        $this->endpoint = 'customer-income-tax/report';

        foreach ($companies as $company) {
            $this->body = [
                'customerId' => Auth::user()->sienge_id,
                'companyId' => $company['id'],
                'year' => $year
            ];

            $this->fetch();
        }
    }
}
