<?php

namespace App\Repositories\Personal;

use App\Repositories\Repository;
use App\User;
use Illuminate\Support\Collection;

class EnterpriseRepository extends Repository
{
    public function first(int $id): ?Collection
    {
        $this->endpoint = 'enterprises/' . $id;

        if ($response = $this->fetch()) {
            return collect($response);
        }

        return null;
    }
}
