<?php

namespace App\Repositories\Personal;

use App\Repositories\Repository;

class AuthRepository extends Repository
{
    public function getUserInfo(string $socialId): ?array
    {
        $this->endpoint = 'customers';
        $this->query = [social_id_type($socialId) => $socialId];

        return $this->fetch()['results'][0] ?? null;
    }
}
