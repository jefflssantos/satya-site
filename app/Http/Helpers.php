<?php

function social_id_type($socialId): string
{
    return strlen($socialId) > 11 ? 'cnpj' : 'cpf';
}

function social_id_numbers($socialId): string
{
    return str_replace(['.', '-', '/'], '', $socialId);
}

function obfuscate_email($email)
{
    $em = explode("@", $email);
    $name = implode('@', array_slice($em, 0, count($em) - 1));
    $len = floor(strlen($name) / 2);

    return substr($name, 0, $len) . str_repeat('*', $len) . "@" . end($em);
}

function url_get_contents($url)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    curl_close($ch);

    return $output;
}
