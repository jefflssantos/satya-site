<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(): View
    {
        $user = Auth::user();

        return view('contact')->with(compact('user'));
    }

    public function store(Request $request): RedirectResponse
    {
       $data = $request->validate([
           'subject' => ['required', 'string'],
           'message' => ['required', 'string']
       ]);

       Mail::to(['email' => config("contact_form.options.{$data['subject']}.email")])
           ->send(new ContactMail($data['subject'], $data['message']));

       return redirect()
           ->route('contact.create')
           ->with(['success' => 'Seu contato foi enviado com sucesso!']);
    }
}
