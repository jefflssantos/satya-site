<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\NewPasswordMail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class ForgotPasswordController extends Controller
{
    public function showLinkRequestForm()
    {
        return view('auth.passwords.email');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $data = $request->validate(['social_id' => ['required', 'string']]);
        $socialId = social_id_numbers($data['social_id']);

        if (!$user = User::where('social_id', $socialId)->first()) {
            return back()
                ->withErrors(['CPF/CNPJ não encontrado.']);
        }

        $password = Str::random(8);
        $user->update(['password' => Hash::make($password)]);

        Mail::to($user)
            ->send(new NewPasswordMail($user, $password));

        return redirect()
            ->route('login')
            ->with('success', 'Sua nova senha foi enviada para o email ' .  obfuscate_email($user->email));
    }
}
