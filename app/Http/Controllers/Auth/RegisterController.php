<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\NewRegistrationMail;
use App\Providers\RouteServiceProvider;
use App\Repositories\Personal\AuthRepository;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = RouteServiceProvider::HOME;

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function register(Request $request)
    {
        $data = $this->validator($request->all())->validate();
        $socialId = social_id_numbers($data['social_id']);

        if (User::where('social_id', $socialId)->count()) {
            return back()
                ->with('success', 'Este usuário já se encontra cadastrado.');
        }

        $userInfo = (new AuthRepository())->getUserInfo($socialId);

        if (is_null($userInfo)) {
            return back()
                ->withErrors(
                    'Seu CPF ou CNPJ não está cadastrado em nossa base de dados. Entre em contato conosco para atualização cadastral e posterior acesso a este canal.'
                );
        }

        if (empty($userInfo['email'])) {
            return back()
                ->withErrors(
                    'Seu e-mail não está cadastrado em nossa base de dados. Entre em contato conosco para atualização cadastral e posterior acesso a este canal.'
                );
        }

        $password = Str::random(8);
        $user = $this->create($userInfo, $password);

        Mail::to($user)
            ->send(new NewRegistrationMail($user, $password));

        $email = obfuscate_email($user->email);

        return redirect()
           ->route('login')
           ->with(
               'success',
               "Parabéns, agora falta pouco para você acessar o sistema: acesse o e-mail {$email} para receber a senha."
           );
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'social_id' => ['required', 'string', 'min:14', 'max:18'],
        ]);
    }

    protected function create(array $data, string $password, string $accountType = 'personal')
    {
        return User::create([
            'name' => $data['name'],
            'email' => strtolower($data['email']),
            'social_id' => $data['cpf'] ?? $data['cnpj'],
            'account_type' => $accountType,
            'sienge_id' => $data['id'],
            'password' => Hash::make($password)
        ]);
    }
}
