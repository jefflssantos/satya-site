<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(): View
    {
        $user = Auth::user();

        return view('profile')->with(compact('user'));
    }

    public function store(Request $request): RedirectResponse
    {
       $data = $request->validate([
           'new_password' => ['required', 'string', 'min:6', 'confirmed'],
           'new_password_confirmation' => ['required', 'string']
       ]);

       $user = Auth::user();
       $user->update(['password' => Hash::make($data['new_password'])]);

       return redirect()
           ->route('profile.show')
           ->with(['success' => 'Sua senha foi atualizada com sucesso!']);
    }
}
