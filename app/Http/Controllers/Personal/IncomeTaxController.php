<?php

namespace App\Http\Controllers\Personal;

use App\Http\Controllers\Controller;
use App\Repositories\Personal\IncomeTaxRepository;
use App\Repositories\Personal\EnterpriseRepository;
use App\Repositories\Personal\SalesContractRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class IncomeTaxController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(): View
    {
        return view('personal.income_tax');
    }

    public function request(Request $request): RedirectResponse
    {
        $companies = $this->getCompanies();

        (new IncomeTaxRepository())->get($request->year, $companies);

        return redirect()
            ->route('personal.income_tax.index')
            ->with(
                'success',
                'Seu extrato está sendo gerado e será enviado para o seu email em instantes.'
            );
    }

    private function getCompanies(): array
    {
        $salesContracts = (new SalesContractRepository(Auth::user()->sienge_id))->get();

        foreach ($salesContracts as $salesContract) {
            $enterprise = (new EnterpriseRepository())->first($salesContract['enterpriseId']);

            $companies[$enterprise['companyId']] = [
                'id' => $enterprise['companyId'],
                'name' => $enterprise['companyName']
            ];
        }

        return $companies ?? [];
    }
}
