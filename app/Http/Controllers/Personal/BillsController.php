<?php

namespace App\Http\Controllers\Personal;

use App\Http\Controllers\Controller;
use App\Repositories\Personal\BillRepository;
use App\Repositories\Personal\SalesContractRepository;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class BillsController extends Controller
{
    protected BillRepository $repository;

    public function __construct(BillRepository $repository)
    {
        $this->middleware('auth');
        $this->repository = $repository;
    }

    public function history(): View
    {
        $installments = $this->repository->paidInstallments(Auth::user()->social_id);

        return view('personal.bills.history')->with(compact('installments'));
    }

    public function index()
    {
        $salesContracts = (new SalesContractRepository(Auth::user()->sienge_id))->get();

        if (! $salesContracts) {
            return redirect()->route('');
        }

        if (! request()->filled('bill')) {
            return redirect()
                ->route('personal.bills.index', ['bill' => $salesContracts[0]['receivableBillId']]);
        }

        $salesContract = $salesContracts->firstWhere('receivableBillId', request()->bill);

        $installments = $this->repository->get(Auth::user()->social_id);
        $installmentStatus = $installments->keyBy('status');

        $installments = $this->filter($installments);

        $latestInstallments = $installments->filter(function ($installment) {
            return $installment['dueDate']->gt(now()->subDays(90))
                && $installment['dueDate']->lt(now()->addDay());
        })->sortByDesc('dueDate');

        $installments = $installments->diffKeys($latestInstallments)
            ->sortBy('dueDate');

        return view('personal.bills.index')
            ->with(compact('installments', 'latestInstallments', 'salesContracts', 'salesContract', 'installmentStatus'));
    }

    public function show(int $bill, int $installment, string $dueDate): Response
    {
        $installmentLink = $this->repository->installmentLink($bill, $installment);

        return response(url_get_contents($installmentLink), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="boleto-' . $dueDate . '.pdf"'
        ]);
    }

    protected function filter(Collection $installments): Collection
    {
        $installments = $installments->where('dueDate', '<', now()->addYear());

        if (request()->filled('bill')) {
            $installments = $installments->where('billReceivableId', request()->bill);
        }

        if (request()->filled('status')) {
            $installments = $installments->where('status', request()->status);
        }

        return $installments;
    }
}
