<?php

namespace App\Http\Middleware;

use App\Repositories\Personal\SalesContractRepository;
use Illuminate\Support\Facades\Auth;
use Closure;

class RedirectIfNoContracts
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $salesContracts = (new SalesContractRepository(Auth::user()->sienge_id))->get();

        if (is_null($salesContracts) && $request->route()->getName() != 'welcome') {
            return redirect()
                ->route('welcome')
                ->with('noSalesContract', true);
        }

        return $next($request);
    }
}
