<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $guarded = [];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getSocialIdTypeAttribute(): string
    {
        return social_id_type($this->social_id);
    }
}
