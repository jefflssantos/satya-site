const mix = require('laravel-mix');

mix.disableNotifications();

mix.browserSync({
    proxy: 'localhost:8000',
    port: 80,
    open: false,
    notify: false,
    files: [
        'resources/views/**/*.php',
        'resources/js/**/*.js',
        'resources/sass/**/*.scss',
    ],
});

mix.sass('resources/sass/bootstrap/bootstrap.scss', 'public/css')
    .sourceMaps(false, 'source-map');

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

if (mix.inProduction()) {
    mix.version();
}
