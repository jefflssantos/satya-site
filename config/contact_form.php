<?php

return [
    'options' => [
        'sugestion' => [
            'title' => 'Sugestão',
            'email' => 'sac@satyasolucoes.com.br'
        ],
        'cadastral-update' => [
            'title' => 'Atualização cadastral',
            'email' => 'sac@satyasolucoes.com.br'
        ],
        'contract' => [
            'title' => 'Contrato',
            'email' => 'comercial@satyasolucoes.com.br'
        ],
        'sales' => [
            'title' => 'Vendas',
            'email' => 'comercial@satyasolucoes.com.br'
        ],
        'discharge' => [
            'title' => 'Quitação',
            'email' => 'financeiro@satyasolucoes.com.br'
        ],
        'negotiation' => [
            'title' => 'Negociação',
            'email' => 'cobrança@satyasolucoes.com.br'
        ],
        '2nd-copy-of-ticket' => [
            'title' => 'Emissão de 2º via de boleto',
            'email' => 'cobrança@satyasolucoes.com.br'
        ],
        'others' => [
            'title' => 'Outros',
            'email' => 'sac@satyasolucoes.com.br'
        ],
    ]
];
