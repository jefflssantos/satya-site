<?php

return [
    'api' => [
        'url' => 'https://api.sienge.com.br/yrconstrucoes/public/api/v1',
        'user' => env('SIENGE_API_USER'),
        'pass' => env('SIENGE_API_PASS'),
        'cache' => env('SIENGE_API_CACHE', true),
    ]
];
