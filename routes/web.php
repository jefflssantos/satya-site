<?php

use Illuminate\Support\Facades\Route;

Route::view('/', 'welcome')->name('welcome');

Route::get('/contato', 'ContactController@create')
    ->name('contact.create');

Route::post('/contato', 'ContactController@store')
    ->name('contact.store');

Route::get('/perfil', 'ProfileController@show')
    ->name('profile.show');

Route::post('/perfil', 'ProfileController@store')
    ->name('profile.store');
