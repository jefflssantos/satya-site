<?php

use Illuminate\Support\Facades\Route;

Route::get('boletos/historico', 'BillsController@history')
    ->name('bills.history');

Route::get('boletos', 'BillsController@index')
    ->name('bills.index');

Route::get('boletos/{bill}/{installment}/{due_date}', 'BillsController@show')
    ->name('bills.show');

Route::get('imposto-de-renda', 'IncomeTaxController@index')
    ->name('income_tax.index');

Route::post('imposto-de-renda', 'IncomeTaxController@request')
    ->name('income_tax.request');

